import { Component, OnInit } from '@angular/core';
import { GraficasService } from '../../services/graficas.service';
import { ChartData, ChartType } from 'chart.js';

@Component({
  selector: 'app-dona-http',
  templateUrl: './dona-http.component.html',
  styles: [
  ]
})
export class DonaHttpComponent implements OnInit {

  constructor(private graficasService: GraficasService) { }

  ngOnInit(): void {

    /* this.graficasService.getUsuariosRedesSociales().subscribe(resp=>{
        this.doughnutChartData={
          labels:Object.keys(resp),
          datasets:[{data:Object.values(resp)}]
        }
      }
    ) */

    this.graficasService.getUsuariosRedesSocialesDonaDate().subscribe(({ labels, datasets }) => {
        this.doughnutChartData = { labels, datasets };
      });

  }

  // Doughnut
  public doughnutChartLabels: string[] = [
    // 'Download Sales', 'In-Store Sales', 'Mail-Order Sales'
  ];

  public doughnutChartData: ChartData<'doughnut'> = {
    labels: this.doughnutChartLabels,
    datasets: [
      // { data: [350, 450, 100] }
    ]
  };
  public doughnutChartType: ChartType = 'doughnut';

}
